import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const thingsApi = createApi({
  reducerPath: 'thingsApi',
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_API_HOST}`,
    credentials: "include"
  }),
  tagTypes: ['Account', 'Things'],
  endpoints: (builder) => ({
    getAccount: builder.query({
      query: () => '/token',
      transformResponse: (response) => response?.account,
      providesTags: ['Account']
    }),
    login: builder.mutation({
      query: (body) => {
        const formData = new FormData()
        formData.append('username', body.username)
        formData.append('password', body.password)
        return {
          url: '/token',
          method: 'POST',
          body: formData,
        }
      },
      invalidatesTags: ['Account', 'Things']
    }),
    signup: builder.mutation({
      query: (body) => {
        return {
          url: '/api/accounts',
          method: 'POST',
          body,
        }
      },
      invalidatesTags: ['Account', 'Things']
    }),
    logout: builder.mutation({
      query: () => ({
        url: '/token',
        method: 'DELETE'
      }),
      invalidatesTags: ['Account', 'Things']
    }),
    deleteThing: builder.mutation({
      query: (id) => ({
        url: `/api/things/${id}`,
        method: 'DELETE'
      }),
      invalidatesTags: (result, error, {id}) => [{type: 'Things', id}]
    }),
    createThing: builder.mutation({
      query: (body) => ({
        url: '/api/things',
        method: 'POST',
        body
      }),
      invalidatesTags: [{ type: 'Things', id: 'LIST' }]
    }),
    getThings: builder.query({
      query: () => '/api/things',
      transformResponse: (response) => response.things,
      providesTags: (result) => {
        const tags = [{ type: 'Things', id: 'LIST' }]
        if (!result) return tags;
        return [
          ...result.map(({id}) => ({type: 'Things', id})),
          ...tags
        ]
      }
    })
  }),
})

export const {
  useGetAccountQuery,
  useLogoutMutation,
  useLoginMutation,
  useSignupMutation,
  useCreateThingMutation,
  useDeleteThingMutation,
  useGetThingsQuery
} = thingsApi;
