import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  name: '',
}

export const newThingSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    handleNameChange: (state, action) => {
      state.name = action.payload
    },
    reset: () => initialState
  },
})

// Action creators are generated for each case reducer function
export const { handleNameChange, reset } = newThingSlice.actions;

export default newThingSlice.reducer;
