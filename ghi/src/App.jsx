import Things from './Things';
import NewThing from './NewThing';
import Login from './Login';
import Signup from './Signup';
import Logout from './Logout';
import { useGetAccountQuery } from './services/things';

function App() {
  const { data: account } = useGetAccountQuery()
  console.log({account});

  const showThings = () => (
    <div className='row'>
        <div className='col'>
          <Things />
        </div>
        <div className='col'>
          <NewThing />
        </div>
      </div>
  )

  const showAuthForms = () => (
    <div className='row'>
      <div className='col'><Login /></div>
      <div className='col'><Signup /></div>
    </div>
  )

  return (
    <div className='container'>

      <h1>Hey, {account?.username || 'Friend'}</h1>
      <hr />

      {account && <Logout />}

      {account ? showThings() : showAuthForms()}
    </div>
  );
}

export default App;
